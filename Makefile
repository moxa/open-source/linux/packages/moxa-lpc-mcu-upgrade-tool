CROSS_COMPILE =
CC := $(CROSS_COMPILE)gcc
STRIP := $(CROSS_COMPILE)strip
CFLAGS:=-Wall -fPIC
LIB := mx-lpc-mcu-upgrade
UTIL := mx-lpc-mcu-upgrade-tool
DESTDIR ?= /

all: clean $(UTIL)

$(LIB).a:  
	gcc $(CFLAGS) $(DEBUG) -c $(LIB).c 
	ar r $(LIB).a $(LIB).o

$(UTIL): $(UTIL).c $(LIB).a lib$(LIB).so
	$(CC) $(DEBUG) -o $(UTIL) $(UTIL).c -L. $(LIB).a
	$(STRIP) -s $(UTIL)

lib%.so: $(LIB).c
	$(CC) -fPIC -shared -I.  -o $@ $< 

install:
	@/usr/bin/install -d $(DESTDIR)/sbin
	@/usr/bin/install $(UTIL) $(DESTDIR)/sbin/$(UTIL)

debug: DEBUG = -DDEBUG
debug: clean $(UTIL)

.PHONY: clean
clean:
	rm -f $(UTIL) *.a *.o *.so

