# Moxa LPC MCU Upgrade Utility for V3000/EXPC-F2000 series

Moxa LPC MCU Upgrade Utility for V3000/EXPC-F2000 series

## Usage

```
Usage:
        mx-lpc-mcu-upgrade-tool [Options]...
Options:
        -f, --file
                Start MCU upgrade from file
        -v, --version
                Get current MCU version
Example:
        mx-lpc-mcu-upgrade-tool -f FB_MCU_V3000_V1.00S03_22060219.bin
        mx-lpc-mcu-upgrade-tool -v
```
