#define MCU_DEVICE "/dev/ttyS2" /* for EXPC-F200W/V3000 MCU UART port */

#define HDR_LEN			5
#define W_CHECK_LEN		9
#define FIN_CMD_LEN		9
#define DATA_HEAD		7
#define MAX_PKT_LEN		150
#define TIMEOUT			5

#define MCU_FIN_UPGRADE		0
#define MCU_START_UPGRADE	1
#define MCU_DEL_UPGRADE		2

#define MCU_BIN_HEADER_BASE	0x2000
#define MCU_BIN_HEADER_INDEX	0x2004

#define MCU_BIN_WRITE_BASE	0x2000
#define MCU_BIN_WRITE_SIZE	128

#define MCU_BIN_HEADER_0	0x48
#define MCU_BIN_HEADER_1	0x80
#define MCU_BIN_HEADER_2	0x47
#define MCU_BIN_HEADER_3	0x4

#define DELAY_START_UPGRADE	300
#define DELAY_SEND_PKT		200
#define DELAY_SEND_CMD		100

/* CMD format: chk bit(2) + cmd id(3) + data len(1) + hdr checksum(1) + data(data len) + data checksum(1) */
/* checksum = 0xff - (data sum & 0xff) */

#define FWR_VERSION_QUERY_HDR	"\x07\xFF\x53\x57\x49"
#define MCU_UPGRADE_MODE_HDR	"\x07\xFF\x53\x46\x55"
#define MCU_FIN_CMD		"\x07\xFF\x53\x46\x55\x01\x0A\x00\xFF"
#define MCU_UPGRADE_RECV_CHECK	"\x06\xFF\x53\x46\x55\x01\x0B\x01\xFE"
#define MCU_DEL_RECV_CHECK	"\x06\xFF\x53\x46\x55\x01\x0B\x02\xFE"
#define MCU_FIN_RECV_CHECK	"\x06\xFF\x53\x46\x55\x01\x0B\x00\xFE"

#define MCU_RECV_CHECK		"\x06\xFF\x53\x46\x55"


void print_cmd(unsigned char *buf, int len);

int mx_get_mcu_fw_version(int delay_ms);
int mx_check_bin_headers(char *file_path);
int mx_set_mcu_upgrade_mode(int mode);
int mx_write_mcu_upgrade_file(char *file_path);
