#include <sys/types.h>
#include <sys/socket.h>
#include <sys/file.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <termios.h>
#include <time.h>
#include "mx-lpc-mcu-upgrade.h"

void print_cmd(unsigned char *buf, int len) {
	int i;
	for (i = 0; i<len; i++)
		printf("0x%02X ", buf[i]);
	printf("\n");
}

void sleep_ms(int milliseconds) {
#if _POSIX_C_SOURCE >= 199309L
	struct timespec ts;
	ts.tv_sec = milliseconds / 1000;
	ts.tv_nsec = (milliseconds % 1000) * 1000000;
	nanosleep(&ts, NULL);
#else
	usleep(milliseconds * 1000);
#endif
}

int validate_cmd_checksum(unsigned char *cmd, int data_len) {
	unsigned char hdr_chk = 0, data_chk = 0;
	int i;

	hdr_chk = cmd[0] + cmd[1] + cmd[2] + cmd[3] + cmd[4] + cmd[5];
	hdr_chk = 0xff - (hdr_chk & 0xff);
	if (hdr_chk != cmd[6]) {
		return 0;
	}

	if(cmd[5]) {
		for (i = DATA_HEAD; i < data_len - 1; i++) {
			data_chk += cmd[i];
		}
		data_chk = 0xff - (data_chk & 0xff);
		if (data_chk != cmd[data_len-1]) {
			return 0;
		}
	}
	return 1;
}

static int set_tty(int fd) {
	struct termios termio;

	tcgetattr(fd, &termio);

	termio.c_iflag = IGNBRK;
	termio.c_oflag = 0;
	termio.c_lflag = 0;
	termio.c_cflag = B9600 | CS8 | CREAD | CLOCAL;
	termio.c_cflag &= ~CRTSCTS;
	termio.c_iflag &= ~(IXON|IXOFF|IXANY);

	termio.c_cc[VMIN] = 1;
	termio.c_cc[VTIME] = 5;
	termio.c_cflag &= ~(PARENB | PARODD);
	termio.c_cflag &= ~CSTOPB;

	tcsetattr(fd, TCSANOW, &termio);
	tcflush(fd, TCIOFLUSH);
	return fd;
}

static int open_tty(char *ttyname) {
	int fd;

	fd = open(MCU_DEVICE, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd < 0) {
		printf("Open %s: %s\n", ttyname, strerror(errno));
		return -1;
	}

	if (set_tty(fd) < 0) {
		printf("Set tty failed: %s\n", strerror(errno));
		close(fd);
		return -1;
	}
	return fd;
}

int make_packet(unsigned char *send_buf, char *cmd_hdr, int data_len, char *data) {
	unsigned char hdr_checksum = 0, data_checksum = 0;
	int i;

	for (i = 0; i < HDR_LEN; i++)
		hdr_checksum += cmd_hdr[i];
	hdr_checksum = 0xff - ((hdr_checksum + (unsigned char)data_len) & 0xff);

	for (i = 0; i < data_len; i++)
		data_checksum += data[i];
	data_checksum = 0xff - (data_checksum & 0xff);

	memcpy(send_buf, cmd_hdr, 5);
	send_buf[HDR_LEN] = data_len;
	send_buf[HDR_LEN+1] = hdr_checksum;
	if (data != NULL) {
		memcpy(send_buf + DATA_HEAD, data, data_len);
		send_buf[DATA_HEAD + data_len] = data_checksum;
		return DATA_HEAD + data_len + 1;
	}
	return DATA_HEAD;
}

int cmd_send(unsigned char *read_buf, unsigned char *send_buf, int pkt_len, int delay_ms) {
	int fd, ret;
	
	fd = open_tty(MCU_DEVICE);
	if (fd < 0) {
		printf("open tty failed\n");
		return -1;
	}
	#ifdef DEBUG
	print_cmd(send_buf, pkt_len);
	#endif
	ret = write(fd, send_buf, pkt_len);
	if (ret < 0) {
		printf("write failed\n");
		return -1;
	}
	sleep_ms(delay_ms);
	memset(read_buf, 0, MAX_PKT_LEN);
	ret = read(fd, read_buf, MAX_PKT_LEN);
	if (ret < 0){
		printf("response failed %s\n", strerror(errno));
		return -1;
	}

	if (!validate_cmd_checksum(read_buf, ret)) {
		printf("response checksum error\n");
		return -1;
	}

	#ifdef DEBUG
	print_cmd(read_buf, ret);
	#endif

	close(fd);

	return ret;
}

int mx_get_mcu_fw_version(int delay_ms) {
	unsigned char send_buf[MAX_PKT_LEN];
	unsigned char recv_buf[MAX_PKT_LEN];
	int ret, plen;

	ret = make_packet(send_buf, FWR_VERSION_QUERY_HDR, 0, NULL);
	ret = cmd_send(recv_buf, send_buf, ret, delay_ms);

	plen = recv_buf[5];
	printf("Current MCU firmware version: %.*s\n", plen, recv_buf+7);

	return ret;
}

int mx_check_bin_headers(char *file_path) {
	int ret, fd;
	unsigned int size;
	char* buf;
	char* headers;

	printf("[CHK HEADER] Check the input MCU bin file: %s\n", file_path);

	fd = open(file_path, O_RDONLY);
	if (!fd) {
		perror("open file failed");
		return 1;
	}

	size = lseek(fd, 0 , SEEK_END);
	printf("[CHK HEADER] Get MCU bin file size: %d bytes\n", size);

	buf = malloc(sizeof(char));
	if ((ret = pread(fd, buf, 1, MCU_BIN_HEADER_INDEX)) == -1) {
		perror("[CHK HEADER] pread failed");
		return 1;
	}

	#ifdef DEBUG
	printf("[CHK HEADER] Get input MCU bin file header index: %x\n", buf[0] & 0xff);
	#endif

	/*
	 * If MCU BIN file header index is return 'd9'
	 * move offset to '0x20d9' = '0x2000+0xd9', then the bin header should be:
	 * 0x20d9, 0x20da, 0x20db, 0x20dc = (Fixed) 48, 80, 47, 04
	*/

	headers = malloc(sizeof(char) * 4);
	if ((ret = pread(fd, headers, 4, MCU_BIN_HEADER_BASE | (0xd9 & 0xff))) == -1) {
		perror("[CHK HEADER] pread failed");
		return 1;
	}

	if (MCU_BIN_HEADER_0 != (headers[0] & 0xff) || MCU_BIN_HEADER_1 != (headers[1] & 0xff) || 
		MCU_BIN_HEADER_2 != (headers[2] & 0xff) || MCU_BIN_HEADER_3 != (headers[3] & 0xff)) {
		perror("[CHK HEADER] Input MCU bin file header error:");
		printf("Input MCU BIN file header 0: %x\n", headers[0] & 0xff);
		printf("Input MCU BIN file header 1: %x\n", headers[1] & 0xff);
		printf("Input MCU BIN file header 2: %x\n", headers[2] & 0xff);
		printf("Input MCU BIN file header 3: %x\n", headers[3] & 0xff);
		return 1;
	}

	close(fd);
	free(buf);
	free(headers);

	printf("[CHK HEADER] MCU bin file header check is passed\n\n");
	return 0;
}

int mx_set_mcu_upgrade_mode(int mode) {
	unsigned char send_buf[MAX_PKT_LEN];
	unsigned char recv_buf[MAX_PKT_LEN];
	unsigned char check_buf[W_CHECK_LEN];
	int ret = 0;
	char buf;

	if (mode == MCU_START_UPGRADE) {
		printf("[SET MODE] Enter MCU upgrade mode\n");
		buf = (unsigned char)mode;
		ret = make_packet(send_buf, MCU_UPGRADE_MODE_HDR, 1, &buf);
		ret = cmd_send(recv_buf, send_buf, ret, DELAY_START_UPGRADE);
		if (ret < 0) {
			printf("[SET MODE] MCU_START_UPGRADE cmd_send is failed\n");
			return -1;
		}
	} else if (mode == MCU_DEL_UPGRADE) {
		printf("[SET MODE] Send delete MCU operation\n");
		buf = (unsigned char)mode;
		ret = make_packet(send_buf, MCU_UPGRADE_MODE_HDR, 1, &buf);
		ret = cmd_send(recv_buf, send_buf, ret, DELAY_START_UPGRADE);
		if (ret < 0) {
			printf("[SET MODE] MCU_DEL_UPGRADE cmd_send is failed\n");
			return -1;
		}
	} else if (mode == MCU_FIN_UPGRADE) {
		printf("[SET MODE] Finish MCU upgrade mode\n");
		memset(send_buf, 0, sizeof(send_buf));
		memcpy(send_buf, "\x07\xFF\x53\x46\x55\x01\x0A\x00\xFF", FIN_CMD_LEN);
		ret = cmd_send(recv_buf, send_buf, FIN_CMD_LEN, DELAY_START_UPGRADE);
		if (ret < 0) {
			printf("[SET MODE] MCU_FIN_UPGRADE cmd_send is failed\n");
			return -1;
		}
	} else {
		printf("Please check input variables. Exit.\n");
		return -1;
	}

	make_packet(check_buf, MCU_RECV_CHECK, 1, &buf);
	ret = memcmp(recv_buf, check_buf, W_CHECK_LEN);
	if (ret != 0) {
		printf("[SET MODE] recv_buf compare is failed\n");
		return -1;
	}

	return ret;
}

int mx_write_mcu_upgrade_file(char* file_path) {
	unsigned char send_buf[MAX_PKT_LEN];
	unsigned char recv_buf[MAX_PKT_LEN];
	unsigned char write_check_buf[W_CHECK_LEN];
	int i, ret, fd, size, pkt_index = 0, data_index = 0, pkt_num = 0;
	char* write_buf;
	char* read_buf;
	char index_buf;

	printf("[WRITE MCU] Input MCU bin file: %s\n", file_path);

	fd = open(file_path, O_RDONLY);
	if (!fd) {
		perror("open file failed");
		return 1;
	}

	/* calulate package number */
	size = lseek(fd, 0 , SEEK_END);
	printf("[WRITE MCU] Get MCU bin file size: %d bytes\n", size);

	/* return ceiling value */
	pkt_num = (size - MCU_BIN_WRITE_BASE) / MCU_BIN_WRITE_SIZE + ((size - MCU_BIN_WRITE_BASE) % MCU_BIN_WRITE_SIZE != 0);
	printf("[WRITE MCU] Assume the total MCU bin file is %d packages\n", pkt_num);

	/*
	* Package format
	* DATA0:  Package index (0:0x00 ~ 255:0xFF)
        * 0x5BFF 
        * 
        * ATTN	ADDR	CMD			LEN	IHCHK	DATA0	DATA1	DATA2	DATA3	…	DATA128	IDCHK
	* 0x07	0xFF	0x53	0x46	0x55	0x81	0x8A	0x00	0x00	0x00	0x00	…	0x00	0xFF
	* ...
	* (if the last package is less than MCU_BIN_WRITE_SIZE, add 0xFF for the remain data, e.g. AA BB CC)
	* 0x07	0xFF	0x53	0x46	0x55	0x81	0x8A	0xAA	0xBB	0xCC	0xFF	…	0xFF	0xFF
	*/

	read_buf = malloc(sizeof(char) * MCU_BIN_WRITE_SIZE);
	write_buf = malloc(sizeof(char) * (MCU_BIN_WRITE_SIZE + 1)); /* DATA0 (Package index) + DATA1~DATA128 */

	for (i = 0; i < pkt_num; i++) {
		printf("Write bin file to MCU: %d/%d...\n", i, pkt_num - 1);

		/* initial buf as 0xFF (from MCU spec) */
		memset(read_buf, ~0, sizeof(char) * MCU_BIN_WRITE_SIZE);
		if ((ret = pread(fd, read_buf, MCU_BIN_WRITE_SIZE, MCU_BIN_WRITE_BASE + pkt_index)) == -1) {
			printf("[WRITE MCU] pread failed\n");
			return 1;
		}

		write_buf[0] = data_index;
		memcpy(write_buf + 1, read_buf, 128);

		ret = make_packet(send_buf, MCU_UPGRADE_MODE_HDR, MCU_BIN_WRITE_SIZE + 1, write_buf);
#ifdef DEBUG
		printf("pkt_index = 0x%x\n", MCU_BIN_WRITE_BASE + pkt_index);
#endif

		/* When write to MCU is succeed, the recv_buf format should be:
		 * ATTN	ADDR	CMD			LEN	IHCHK	DATA0			IDCHK
		 * 0x06	0xFF	0x53	0x46	0x55	0x01	0x0B	0x00	...		0xFF
		 * 0x06	0xFF	0x53	0x46	0x55	0x01	0x0B	0x01	...		0xFF
		 * 0x06	0xFF	0x53	0x46	0x55	0x01	0x0B	0x02	...		0xFF
		 * ... and so on
		 * 0x06	0xFF	0x53	0x46	0x55	0x01	0x0B	0xAA	...	0xFF	0xFF
		 * check recv_buf format, if okay, and go next send.
		 */

		ret = cmd_send(recv_buf, send_buf, ret, DELAY_SEND_PKT);
		if (ret < 0) {
			printf("[WRITE MCU] cmd_send is failed\n");
			return -1;
		}

		index_buf = (unsigned char)data_index;
		make_packet(write_check_buf, MCU_RECV_CHECK, 1, &index_buf);
		ret = memcmp(recv_buf, write_check_buf, W_CHECK_LEN);
		if (ret != 0) {
			printf("[WRITE MCU] recv_buf compare is failed\n");
			return -1;
		}

		pkt_index += MCU_BIN_WRITE_SIZE;
		data_index++;
		sleep_ms(DELAY_SEND_PKT);
	} 

	close(fd);
	free(read_buf);
	free(write_buf);

	return 0;
}

