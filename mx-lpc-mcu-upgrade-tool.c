#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <time.h>
#include "mx-lpc-mcu-upgrade.h"

void usage()
{
	printf("Usage:\n");
	printf("	mx-lpc-mcu-upgrade-tool [Options]...\n");
	printf("Options:\n");
	printf("	-f, --file\n");
	printf("		Start MCU upgrade from file\n");
	printf("	-v, --version\n");
	printf("		Get current MCU version\n");
	printf("Example:\n");
	printf("	mx-lpc-mcu-upgrade-tool -f FB_MCU_V3000_V1.00S03_22060219.bin\n");
	printf("	mx-lpc-mcu-upgrade-tool -v\n");
	printf( "\n");
}

int main(int argc, char *argv[])
{
	int c, ret;
	char response;
	struct option *options;

	struct option long_options[] = {
		{"help", no_argument, 0, 'h'},
		{"file", no_argument, 0, 'f'},
		{"version", no_argument, 0, 'v'},
		{0, 0, 0, 0}
	};

	if (argc <= 1 || argc > 3) {
		usage();
		return 1;
	}

	c = getopt_long(argc, argv, "hfv", long_options, NULL);
	switch (c) {
		case 'h':
			usage();
			exit(0);
		case 'f':
			if (argv[optind]) {
				if (argc == 3) {
					if (access(argv[optind], F_OK) != 0) {
						printf("%s does not exist\n", argv[optind]);
						return 1;
					}
					if (mx_check_bin_headers(argv[optind]) != 0) {
						printf("MCU bin file header check is failed\n");
						return 1;
					}

					/* ensure to upgrade or not */
					printf("Ready to upgrade from MCU file path: %s\n", argv[optind]);
					printf("Please DO NOT power off your machine when upgrading...\n");
					printf("Do you want to continue? (y/n)\n");
					response = getchar();
					if (response == '\n') {
						return 1;
					} else if (response == 'N' || response == 'n') {
						return 1;
					} else if (response == 'Y' || response == 'y') {
						printf("==============================\n");
						printf("Start to upgrade MCU procedure\n");
						printf("==============================\n");
					} else {
						return 1;
					}

					/* enter MCU upgrade mode */
					if (mx_set_mcu_upgrade_mode(MCU_START_UPGRADE) != 0) {
						printf("Set MCU enter upgrade mode is failed\n");
						return 1;
					}
					/* wait 500 ms for MCU enter upgrade mode */
					usleep(500000);
					if (mx_set_mcu_upgrade_mode(MCU_DEL_UPGRADE) != 0) {
						printf("Send MCU delete old fw command is failed\n");
						return 1;
					}
					/* wait 100 ms for MCU delete old fw */
					usleep(100000);
					if (mx_write_mcu_upgrade_file(argv[optind]) != 0) {
						printf("Write MCU bin file is failed\n");
						return 1;
					}
					if (mx_set_mcu_upgrade_mode(MCU_FIN_UPGRADE) != 0) {
						printf("Set MCU finished upgrade mode is failed\n");
						return 1;
					}

					printf("=====================================\n");
					printf("The procedure of MCU upgrade is done.\n");
					usleep(1000000);
					/* get current MCU fw version after upgrade procedure */
					if (mx_get_mcu_fw_version(DELAY_SEND_CMD) < 0) {
						printf("Send get MCU fw version is failed\n");
						return 1;
					}
					printf("Please reboot system for these changes to take MCU effect\n");
				} else {
					printf("Please check input variables. Exit.\n");
					return 1;
				}
			}
			break;
		case 'v':
			/* get current MCU fw version */
			if (mx_get_mcu_fw_version(DELAY_SEND_CMD) < 0) {
				printf("Send get MCU fw version is failed\n");
				return 1;
			}
			exit(0);
		default:
			usage();
			exit(99);
	}

	return 0;
}
